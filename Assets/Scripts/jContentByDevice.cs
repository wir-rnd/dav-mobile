﻿using System.Collections.Generic;

public class jContentByDevice
{
    public string SerialNumber { get; set; }
    public List<jContent> Contents { get; set; }
}