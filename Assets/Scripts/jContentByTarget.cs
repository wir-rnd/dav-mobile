﻿using System.Collections.Generic;

public class jContentByTarget
{
	public string TargetID { get; set; }

	public List<jContent> Contents { get; set; }
}
