﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DAV;

namespace DAV
{
    public class Content : MonoBehaviour
    {
        GameObject mContent = null;
        int DownloadCount = 0;
        //List<string> list = new List<string>();

        protected DAV.MessageBox davMessage;
        protected DAV.GUI davGUI;
        protected DAV.News davNews;
        protected DAV.Tracking davTracking;
        protected DAV.Content davContent;
        protected DAV.LoadManager loadMgr;
        protected DAV.Global Instance;

        void Awake()
        {
            mContent = GameObject.Find("Contents");
            Instance = (DAV.Global)FindObjectOfType(typeof(DAV.Global));
            loadMgr = (DAV.LoadManager)FindObjectOfType(typeof(DAV.LoadManager));
            davMessage = (DAV.MessageBox)FindObjectOfType(typeof(DAV.MessageBox));
            davGUI = (DAV.GUI)FindObjectOfType(typeof(DAV.GUI));
            davNews = (DAV.News)FindObjectOfType(typeof(DAV.News));
            davTracking = (DAV.Tracking)FindObjectOfType(typeof(DAV.Tracking));
            davContent = (DAV.Content)FindObjectOfType(typeof(DAV.Content));
        }

        public IEnumerator Run()
        {
            this.davMessage.SimpleShow("DAV Content Run");
            Debug.Log("Content.Run/Build");

            // built all content had been downloaded
            SimpleJSON.JSONNode Contents = DAV.Global.mContents["Contents"];
            for (int i = 0; i < Contents.Count; i++)
            {
                SimpleJSON.JSONNode ContentDetails = Contents[i]["ContentDetail"];
                string content_name = Contents[i]["Name"];
                //Debug.Log(content_name);
                for (int j = 0; j < ContentDetails.Count; j++)
                {
                    string name = ContentDetails[j]["Name"];
                    string ext = name.Split('.')[1];
                    if (ext.Equals("unity3d"))
                    {
                        if (!DAV.Global.Assets.ContainsKey(ContentDetails[j]["ContentDetailID"]))
                        {
                            DAV.Global.CURRENT_ASSET_PATH = DAV.Global.CONTENTS_PATH + "/" + content_name + "/";
                            yield return StartCoroutine(BuildAsset(ContentDetails[j]["ContentDetailID"], content_name, name));
                            yield return StartCoroutine(AssemblyAssetToTarget(Contents[i]["Name"], Contents[i]["TargetName"], Contents[i]["pos"], Contents[i]["rot"], Contents[i]["scale"]));
                        }
                        else
                        {
                            yield return StartCoroutine(AssemblyAssetToTarget(content_name, Contents[i]["TargetName"], Contents[i]["pos"], Contents[i]["rot"], Contents[i]["scale"]));
                        }
                    }
                }
            }
            yield return null;
        }

        public IEnumerator InitDownload(string name)
        {
            this.davMessage.SimpleShow("DAV Content Init Download");
            Dictionary<int, SimpleJSON.JSONNode> DistinctContent = new Dictionary<int, SimpleJSON.JSONNode>();
            List<int> DistinctContentID = new List<int>();
            List<string> list = new List<string>();
            SimpleJSON.JSONNode Contents = DAV.Global.mContents["Contents"];
            string path = string.Empty;

            //Debug.Log("init download " + Contents.Count);
            
            // sync directory
            List<string> wantToSync = new List<string>();
            for (int i = 0; i < Contents.Count; i++)
            {
                wantToSync.Add(Contents[i]["Name"]);
                if (!DistinctContent.ContainsKey(int.Parse(Contents[i]["ContentID"])))
                {
                    DistinctContent.Add(int.Parse(Contents[i]["ContentID"]), Contents[i]);
                    DistinctContentID.Add(int.Parse(Contents[i]["ContentID"]));
                }
            }
            Utilities.SyncDirectory(wantToSync);
            // sync directory

            for (int i = 0; i < DistinctContentID.Count; i++)
            {
                path = DAV.Global.CONTENTS_PATH + "/" + DistinctContent[DistinctContentID[i]]["Name"];
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                SimpleJSON.JSONNode ContentDetails = DistinctContent[DistinctContentID[i]]["ContentDetail"];
                for (int j = 0; j < ContentDetails.Count; j++)
                {
                    string ctn_name = DistinctContent[DistinctContentID[i]]["Name"] + "/" + ContentDetails[j]["Name"];

                    DAV.Global.StateCheck(ctn_name);
                    DAV.Global.StateReset(ctn_name);
                    path = DAV.Global.CONTENTS_PATH + "/" + DistinctContent[DistinctContentID[i]]["Name"] + "/" + ContentDetails[j]["Name"];
                    //Debug.Log(path);
                    if(!Utilities.FileCheck(path, ContentDetails[j]["MD5Checksum"]))
                    {
                        //StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "contents/" + name, null, name, WWWType.FILE, DAV.Global.TRACKINGS_PATH + "/" + name));
                        StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "contents/" + ctn_name, null, ctn_name, WWWType.FILE, DAV.Global.CONTENTS_PATH + "/" + ctn_name));
                        list.Add(ctn_name);
                        DownloadCount++;

                        if (DAV.Global.Assets.ContainsKey(ContentDetails[j]["ContentDetailID"]))
                            DAV.Global.Assets.Remove(ContentDetails[j]["ContentDetailID"]);

                        DestroyContent(DistinctContent[DistinctContentID[i]]["Name"]);
                    }
                }
            }

            davGUI.IncrementLoadList(list);
            if (this.DownloadCount > 0)
            {
                Debug.Log("-0---" + DownloadCount);
                yield return StartCoroutine(Instance.IsDownloadDone(list, name));
                this.DownloadCount = 0;
                StartCoroutine(InitDownload(name));
                //this.Download();
            }
        }

        /*void Download()
        {
            for (int i = 0; i < this.list.Count; i++)
            {
                //Debug.Log(DAV.Global.WEB_SERVICE_URL + "contents/" + list[i] + " -- " + DAV.Global.CONTENTS_PATH + "/" + list[i]);
                StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "contents/" + list[i], null, list[i], WWWType.FILE, DAV.Global.CONTENTS_PATH + "/" + list[i]));
                davGUI.IncrementLoadList(list[i]);
            }
        }*/

        public IEnumerator Stop()
        {
            yield return null;
        }

        IEnumerator BuildAsset(string id, string content_name, string unity3d_filename)
        {
            this.davMessage.SimpleShow("Build Asset " + id + " " + content_name);
            string path = DAV.Global.CONTENTS_PATH + "/" + content_name + "/" + unity3d_filename;
            //AssetBundle ab = AssetBundle.CreateFromMemoryImmediate(File.ReadAllBytes(path));
            AssetBundleCreateRequest abcr = AssetBundle.CreateFromMemory(File.ReadAllBytes(path));
            AssetBundle ab = null;
            yield return abcr;

            if (abcr.isDone)
            {
                ab = abcr.assetBundle;
                if (!DAV.Global.Assets.ContainsKey(id))
                    DAV.Global.Assets.Add(id, ab);
                else
                    DAV.Global.Assets[id] = ab;

                yield return new WaitForSeconds(0.5f);

                if (ab == null)
                {
                    davMessage.Show("Build Asset", "Failed to Build Asset, Asset is NULL", true, 3);
                }

                GameObject go = GameObject.Instantiate(DAV.Global.Assets[id].mainAsset) as GameObject;
                go.name = content_name;
                go.transform.parent = mContent.transform;
                go.SetActive(false);

                GUITexture gtex = go.AddComponent<GUITexture>();

                gtex.texture = (Texture)DAV.Global.Assets[id].LoadAsset("DAV_splash", typeof(Texture)) as Texture;



                //TextAsset ta = (TextAsset)DAV.Global.Assets[id].LoadAsset("Sample", typeof(TextAsset)) as TextAsset;
                //Debug.Log(ta.bytes.Length);
                //File.WriteAllBytes(DAV.Global.CONTENTS_PATH + "/" + content_name + "/" + "Sample.dll", ta.bytes);
                //System.Reflection.Assembly.
                //var assembly = System.Reflection.Assembly.Load(ta.bytes);
                //var type = assembly.GetType("Test");
                //go.AddComponent(type);
                //Debug.Log(ta.GetType());
                //Debug.Log(ta.bytes.Length);
                //Debug.Log(ta.name);
                //Debug.Log(ta.text);

                DAV.Global.Assets[id].Unload(false);
                yield return new WaitForSeconds(1f);
            }
            else
            {
                davMessage.Show("Build Asset", "Create Request failed, re-create...", false, 3);
                yield return new WaitForSeconds(3);
                StartCoroutine(BuildAsset(id, content_name, unity3d_filename));
            }

            
        }

        IEnumerator AssemblyAssetToTarget(string content_name, string target_name, string pos, string rot, string scale)
        {
            this.davMessage.SimpleShow("Assembling Asset " + content_name + " to " + target_name);
            Transform t = GameObject.Find("Targets").transform.Find(target_name);
            if (t != null)
            {
                GameObject go = GameObject.Instantiate(mContent.transform.Find(content_name).gameObject) as GameObject;
                go.name = content_name;

                go.transform.parent = GameObject.Find("Targets").transform.Find(target_name);
                string[] _pos = pos.Split(';');
                string[] _rot = rot.Split(';');
                string[] _scale = scale.Split(';');

                go.transform.localPosition = new Vector3(float.Parse(_pos[0]), float.Parse(_pos[1]), float.Parse(_pos[2]));
                go.transform.localEulerAngles = new Vector3(float.Parse(_rot[0]), float.Parse(_rot[1]), float.Parse(_rot[2]));
                //go.transform.localRotation.SetEulerAngles(new Vector3(_rot[0], _rot[1], _rot[2]));
                go.transform.localScale = new Vector3(float.Parse(_scale[0]), float.Parse(_scale[1]), float.Parse(_scale[2]));
                go.SetActive(false);
                //Debug.Log("AssemblyObjectToTarget(" + content_name + ", " + target_name + ")");
            }
            else
            {
                davMessage.Show("Assembly Content to Target", "Assembly " + content_name + " to Target " + target_name + " has failed", true, 3);
            }
            yield return new WaitForEndOfFrame();
        }

        public void SetScale(string content_name, float x, float y, float z)
        {
            mContent.transform.Find(content_name).localScale = new Vector3(x, y, z);
        }

        public void SetPosition(string content_name, float x, float y, float z)
        {
            mContent.transform.Find(content_name).localPosition = new Vector3(x, y, z);
        }

        public void SetRotation(string content_name, float x, float y, float z)
        {
            Quaternion q = Quaternion.Euler(x, y, z);
            //q.eulerAngles = new Vector3(x, y, z);

            mContent.transform.Find(content_name).localRotation = q;
        }

        public void DestroyContentInTarget(string content_name, string target_name)
        {
            UnityEngine.Object.Destroy(GameObject.Find("Targets/" + target_name).transform.Find(content_name).gameObject);
        }

        public void DestroyContent(string content_name)
        {
            try
            {
                UnityEngine.Object.Destroy(mContent.transform.Find(content_name).gameObject);
            }
            catch { }
        }
    }
}