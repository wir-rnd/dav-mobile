﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

namespace DAV
{
    public enum StateType
    {
        IDLE,
        LOAD_SUCCESS,
        LOAD_FAILED,
        PARSE_SUCCESS,
        PARSE_FAILED,
        FILE_STORED,
        FILE_BROKEN,
        IS_REQUESTED
    }

    public enum WWWType
    {
        FILE,
        JSON
    }


    public class Global : DAV.Behaviour
    {
        public static bool IsDAVPlaying = false;
        public static string CURRENT_ASSET_PATH = string.Empty;

        //public static string WEB_SERVICE_URL = "http://10.0.0.160/dav/";
        //public static string WEB_SERVICE_URL = "http://10.0.0.241/dav/";
        public static string WEB_SERVICE_URL = "http://182.253.226.170/dav/";
        //public static string WEB_SERVICE_URL = "http://localhost/dav/";
        public static string DISK_PATH = string.Empty;
        public static string CONTENTS_PATH = string.Empty;
        public static string TRACKINGS_PATH = string.Empty;
        public static string NEWS_PATH = string.Empty;
        public static string OFFLINE_DAT_PATH = string.Empty;
        public static string STREAM_ASSET_PATH = string.Empty;

        public static Dictionary<string, StateType> State = new Dictionary<string,StateType>();
        public static Dictionary<string, WWW> WWWList = new Dictionary<string, WWW>();
        public static Dictionary<string, AssetBundle> Assets = new Dictionary<string, AssetBundle>();
        public static string DAT_TYPE = ".dat";
        public static string XML_TYPE = ".xml";


        //public DAVTracking davTracking = null;

        public static JSONNode mConfig;
        public static JSONNode mNews;
        public static JSONNode mTrackings;
        public static JSONNode mContents;

        public static bool test = true;

        //Message message = (Message)FindObjectOfType(typeof(Message));
        DAV.MessageBox davMsg;

        public void Test()
        {
            Debug.Log("test");
            Global.test = false;
        }

        void Awake()
        {
            DISK_PATH = Application.persistentDataPath;
            CONTENTS_PATH = DISK_PATH + "/Contents";
            TRACKINGS_PATH = DISK_PATH + "/Trackings";
            NEWS_PATH = DISK_PATH + "/News";
            OFFLINE_DAT_PATH = DISK_PATH + "/OfflineDat";
            STREAM_ASSET_PATH = DISK_PATH + "/QCAR";
            davMsg = (DAV.MessageBox)FindObjectOfType(typeof(DAV.MessageBox));
        }

        public void CloseMessageButton()
        {
            //DAV.MessageBox davMsg = (DAV.MessageBox)FindObjectOfType(typeof(DAV.MessageBox));
            davMsg.Close();
            Debug.Log("CloseMessageButton");
        }

        public void CheckLocalDirectory()
        {
            if (!Directory.Exists(CONTENTS_PATH))
            {
                Directory.CreateDirectory(CONTENTS_PATH);
            }
            //else
            //msg.Show("AAA", Directory.Exists(DownloadManager.CONTENTS_PATH).ToString(), true);

            if (!Directory.Exists(TRACKINGS_PATH))
            {
                Directory.CreateDirectory(TRACKINGS_PATH);
            }

            if (!Directory.Exists(NEWS_PATH))
            {
                Directory.CreateDirectory(NEWS_PATH);
            }

            if (!Directory.Exists(OFFLINE_DAT_PATH))
            {
                Directory.CreateDirectory(OFFLINE_DAT_PATH);
            }
        }

        public void QuitApps()
        {
            Application.Quit();
        }

        public void ShowRegistryNumber()
        {
            davMsg.Show("DAV Device Registry Number", SystemInfo.deviceUniqueIdentifier.ToString(), true, 0);
        }



        public static void StateCheck(string name)
        {
            if (!DAV.Global.State.ContainsKey(name)) DAV.Global.State.Add(name, StateType.IDLE);
            
            
        }

        public static void StateReset(string name)
        {
            if (DAV.Global.State.ContainsKey(name)) DAV.Global.State[name] = StateType.IDLE;
            //if (DAV.Global.WWWList.ContainsKey(name)) DAV.Global.WWWList.Remove(name);
        }

        //Dictionary<string, Dictionary<int, StateType>> TotalIsDone = new Dictionary<string, Dictionary<int, StateType>>();
        Dictionary<string, int> TotalIsDone = new Dictionary<string, int>();
        //public static List<string> DownloadProgressList = new List<string>();
        //public static string DownloadProgressState = string.Empty;

        public IEnumerator IsDownloadDone(List<string> DownloadProgressList, string DownloadProgressState)
        {
            Debug.Log("Total Must be Done " + DownloadProgressList.Count);
            if (!TotalIsDone.ContainsKey(DownloadProgressState)) TotalIsDone.Add(DownloadProgressState, 0);
            else TotalIsDone[DownloadProgressState] = 0;

            while (DownloadProgressList.Count > TotalIsDone[DownloadProgressState])
            {
                yield return StartCoroutine(TotalDone(DownloadProgressList, DownloadProgressState));
            }
            DownloadProgressList.Clear();
            yield return new WaitForSeconds(2);
            //StartCoroutine(wantToExec);
            yield return new WaitForEndOfFrame();
        }

        IEnumerator TotalDone(List<string> DownloadProgressList, string DownloadProgressState)
        {
            TotalIsDone[DownloadProgressState] = 0;
            for (int i = 0; i < DownloadProgressList.Count; i++)
            {
                if (DAV.Global.WWWList[DownloadProgressList[i]].isDone && string.IsNullOrEmpty(DAV.Global.WWWList[DownloadProgressList[i]].error))
                {
                    //Debug.Log("TotalDone Increment " + list[i] + " " + DAV.Global.WWWList[list[i]].progress + " " + DAV.Global.WWWList[list[i]].isDone);
                    TotalIsDone[DownloadProgressState]++;
                }
            }
            Debug.Log("Total Done : " + TotalIsDone[DownloadProgressState]);
            yield return new WaitForEndOfFrame();
        }

    }
}