﻿using UnityEngine;
using System.Collections;
using DAV;

namespace DAV
{
    public class MessageBox : DAV.Global
    {
        static GameObject root;
        static UnityEngine.UI.Text title;
        static UnityEngine.UI.Text message;
	    // Use this for initialization
        public MessageBox()
        { }

        public void SimpleShow(string msg)
        {
            MessageBox.root = GameObject.Find("UI/Canvas/SimpleMessage");
            MessageBox.message = root.transform.Find("Text").gameObject.GetComponent<UnityEngine.UI.Text>();
            MessageBox.message.text = msg;
            StartCoroutine(MessageBox.GenerateMsg(false, 3));
        }

        public void Show(string title, string msg, bool closeBtn)
        {
            MessageBox.root = GameObject.Find("UI/Canvas/Message");
            MessageBox.title = root.transform.Find("Title").gameObject.GetComponent<UnityEngine.UI.Text>();
            MessageBox.message = root.transform.Find("Text").gameObject.GetComponent<UnityEngine.UI.Text>();
            MessageBox.title.text = title;
            MessageBox.message.text = msg;
            StartCoroutine(MessageBox.GenerateMsg(closeBtn, 3));
        }

        public void Show(string title, string msg, bool closeBtn, int second)
        {
            MessageBox.root = GameObject.Find("UI/Canvas/Message");
            MessageBox.title = root.transform.Find("Title").gameObject.GetComponent<UnityEngine.UI.Text>();
            MessageBox.message = root.transform.Find("Text").gameObject.GetComponent<UnityEngine.UI.Text>(); 
            MessageBox.title.text = title;
            MessageBox.message.text = msg;
            StartCoroutine(MessageBox.GenerateMsg(closeBtn, second));
        }

        /*public IEnumerator Show(string title, string msg, bool closeBtn, int second)
        {
            yield return StartCoroutine(Message.GenerateMsg(title, msg, closeBtn, second));
        }*/


        static IEnumerator GenerateMsg(bool closeBtn, int second)
        {
            
            //Message.root.transform.Find("MessageBoxPanel").transform.localPosition = new Vector3(0, float.Parse((-(Screen.height / 2) + 55).ToString()), 0);
            MessageBox.root.SetActive(true);
            try
            {
                MessageBox.root.transform.Find("CloseButton").gameObject.SetActive(closeBtn);
            }
            catch { }

            if(second>0)
            {
                yield return new WaitForSeconds(second);
                MessageBox.root.SetActive(false);
                //MessageBox.title.text = string.Empty;
                //MessageBox.message.text = string.Empty;
            }
            else
            {
                try
                {
                    MessageBox.root.transform.Find("CloseButton").gameObject.SetActive(true);
                }
                catch { }
            }
        }

        public void Close()
        {
            MessageBox.root = GameObject.Find("UI/Canvas/Message");
            MessageBox.root.SetActive(false);
        }
    }
}