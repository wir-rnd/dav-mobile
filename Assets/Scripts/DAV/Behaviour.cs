﻿using UnityEngine;
using System.Collections;

namespace DAV
{
    public class Behaviour : MonoBehaviour
    {
        protected DAV.MessageBox davMessage;
        protected DAV.GUI davGUI;
        protected DAV.News davNews;
        protected DAV.Tracking davTracking;
        protected DAV.Content davContent;
        protected DAV.LoadManager loadMgr;
        protected DAV.Global Instance;

        void Awake()
        {
            try
            {
                Instance = (DAV.Global)FindObjectOfType(typeof(DAV.Global));
                loadMgr = (DAV.LoadManager)FindObjectOfType(typeof(DAV.LoadManager));
                davMessage = (DAV.MessageBox)FindObjectOfType(typeof(DAV.MessageBox));
                davGUI = (DAV.GUI)FindObjectOfType(typeof(DAV.GUI));
                davNews = (DAV.News)FindObjectOfType(typeof(DAV.News));
                davTracking = (DAV.Tracking)FindObjectOfType(typeof(DAV.Tracking));
                davContent = (DAV.Content)FindObjectOfType(typeof(DAV.Content));
            }
            catch (System.Exception E)
            {
                Debug.Log(E.ToString());
            }
        }

    }

}