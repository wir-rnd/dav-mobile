﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DAV;

namespace DAV
{
    public class Tracking : DAV.Behaviour
    {
        public bool IsTrackingRun = false;
        int DownloadCount = 0;
        public IEnumerator InitDownload(string name) // only in tracking, tracking file is very important module that must run
        {
            Debug.Log("Tracking.InitDownload");
            List<string> list = new List<string>();
            SimpleJSON.JSONNode dTracking = DAV.Global.mTrackings["Tracking"];
            Debug.Log(dTracking.Count);
            for (int i = 0; i < dTracking.Count; i++)
            {
                string dat = dTracking[i]["name"] + DAV.Global.DAT_TYPE;
                string xml = dTracking[i]["name"] + DAV.Global.XML_TYPE;
                //Debug.Log(DAV.Global.State[dat]);

                DAV.Global.StateCheck(dat);
                DAV.Global.StateReset(dat);

                string path_dat = DAV.Global.TRACKINGS_PATH + "/" + dat;
                if (!Utilities.FileCheck(path_dat, dTracking[i]["dat_md5"]))
                {
                    //url = DAV.Global.WEB_SERVICE_URL + "targets/" + dTracking[i]["name"] + "/" + dat;
                    list.Add(dat);
                    //StartCoroutine(loadMgr.Run(url, null, dat, WWWType.FILE, path_dat));
                    DownloadCount++;
                    string dir = dat.Split('.')[0];
                    StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "targets/" + dir + "/" + dat, null, dat, WWWType.FILE, DAV.Global.TRACKINGS_PATH + "/" + dat));
                    //davGUI.IncrementLoadList(dat);
                    Debug.Log(DAV.Global.WEB_SERVICE_URL + "targets/" + dir + "/" + dat);
                }

                DAV.Global.StateCheck(xml);
                DAV.Global.StateReset(xml);
                string path_xml = DAV.Global.TRACKINGS_PATH + "/" + xml;
                if (!File.Exists(path_xml))
                {
                    //url = DAV.Global.WEB_SERVICE_URL + "targets/" + dTracking[i]["name"] + "/" + xml;
                    list.Add(xml);
                    DownloadCount++;
                    //Debug.Log(DAV.Global.WEB_SERVICE_URL + "target/xml/" + dTracking[i]["target_id"]);
                    StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "target/xml/" + dTracking[i]["target_id"], null, xml, WWWType.FILE, path_xml));
                    //StartCoroutine(loadMgr.Run(url, null, xml, WWWType.FILE, path_xml));
                    
                    //davGUI.IncrementLoadList(xml);
                }
            }

            davGUI.IncrementLoadList(list);
            if (DownloadCount > 0)
            {
                yield return StartCoroutine(Instance.IsDownloadDone(list, name));
                DownloadCount = 0;
                StartCoroutine(InitDownload(name));
            }
        }

        /*void Download(List<string> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                string dir = list[i].Split('.')[0];
                StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + "targets/" + dir + "/" + list[i], null, list[i], WWWType.FILE, DAV.Global.TRACKINGS_PATH + "/" + list[i]));
                davGUI.IncrementLoadList(list[i]);
            }
        }*/

        public IEnumerator Run()
        {
            Debug.Log("Tracking.Run");
            IsTrackingRun = true;
            GameObject mTargetsObject = GameObject.Find("Targets");
            //msg.Show("Total Tracking", DownloadManager.mTarget.Tracking.Count.ToString(), true);
            //yield return new WaitForSeconds(3);

            GameObject cam = GameObject.Find("ARCamera");
            //1. Remove DataSetLoad in ARCamera
            Destroy(cam.GetComponent<Vuforia.DataSetLoadBehaviour>());

            //2. Copy All tracking file in disk to stream asset
            List<string> target_list = new List<string>();
            List<string> target_list_exroot = new List<string>();
            //string ex_path = Application.persistentDataPath + "/Targets/";
            target_list_exroot.Add(DAV.Global.TRACKINGS_PATH + "/");

            for (int i = 0; i < DAV.Global.mTrackings["Tracking"].Count; i++)
            {
                string src = DAV.Global.TRACKINGS_PATH + "/" + DAV.Global.mTrackings["Tracking"][i]["name"];
                if (File.Exists(src + DAV.Global.DAT_TYPE) && File.Exists(src + DAV.Global.XML_TYPE))
                {
                    target_list.Add(DAV.Global.mTrackings["Tracking"][i]["name"]);
                }
                else
                {
                    //StopCoroutine(Run());
                    yield break;
                }
            }

            Vuforia.ObjectTracker its = (Vuforia.ObjectTracker)Vuforia.TrackerManager.Instance.GetTracker<Vuforia.ObjectTracker>();
            //Vuforia.ObjectTarget its = (Vuforia.ObjectTarget)Vuforia.TrackerManager.Instance.GetTracker<Vuforia.ImageTra>();
            //Vuforia.TrackerManager.Instance.GetStateManager().
            IEnumerable<Vuforia.TrackableBehaviour> trackableBehaviours = Vuforia.TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
            Vuforia.StateManager stateManager = (Vuforia.StateManager)Vuforia.TrackerManager.Instance.GetStateManager();
            if (mTargetsObject.transform.childCount > 0)
            {
                its.DestroyAllDataSets(false);
                // 4. Clear All Image Target Object in scene include trackable in memory
                for (int i = 0; i < mTargetsObject.transform.childCount; i++)
                {
                    Vuforia.ImageTargetBehaviour itb = mTargetsObject.transform.GetChild(i).gameObject.GetComponent<Vuforia.ImageTargetBehaviour>();
                    stateManager.DestroyTrackableBehavioursForTrackable(itb.Trackable, true);
                }

            }


            yield return new WaitForSeconds(2);

            // 5. Put all tracking list in data set load behaviour
            Vuforia.DataSetLoadBehaviour dslb = cam.AddComponent<Vuforia.DataSetLoadBehaviour>();
            dslb.mDataSetsToLoad = target_list;
            dslb.mDataSetsToActivate = target_list;
            dslb.mExternalDatasetRoots = target_list_exroot;
            dslb.LoadDatasets();

            for (int i = 0; i < target_list.Count; i++)
            {
                //Debug.Log(target_list[i] + " " + target_list_exroot[i]);
            }


            IEnumerable<Vuforia.DataSet> ieds = its.GetDataSets();
            foreach (Vuforia.DataSet dataSet in ieds)
            {
                foreach (Vuforia.TrackableBehaviour trackableBehaviour in trackableBehaviours)
                {
                    // check if the Trackable of the current Behaviour is part of this dataset
                    if (dataSet.Contains(trackableBehaviour.Trackable))
                    {
                        GameObject go = trackableBehaviour.gameObject;
                        go.AddComponent<DAV.TrackableEventHandler>();
                        trackableBehaviour.name = trackableBehaviour.Trackable.Name;
                        trackableBehaviour.transform.parent = mTargetsObject.transform;
                    }
                }
            }

            yield return new WaitForSeconds(1);
            IsTrackingRun = false;
            //yield return StartCoroutine(ReloadContent());
        }

    }
}