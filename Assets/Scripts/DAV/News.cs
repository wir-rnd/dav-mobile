﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DAV;
using System.IO;

namespace DAV
{
    public class News : DAV.Behaviour
    {
        public float Delay = 4f;
        public GameObject btNextPrev;
        private GameObject _items;
        private GameObject _this;
        private bool isRun = false;

        void Awake()
        {
            _this = GameObject.Find("Slides");
            btNextPrev = GameObject.Find("BtNextPrev");
        }

        void OnEnable()
        {
            btNextPrev.SetActive(true);
            //print("Enable,,,,"+QuizUI.activeSelf);
            //StartCoroutine(Slide());
        }


        /*void OnDisable()
        {
            btNextPrev.SetActive(false);
            //_items.SetActive(false);
            //print("Disable");
            LeanTween.moveLocalX(GameObject.Find("News/Items"), 1f, 1f).setEase(LeanTweenType.easeOutQuint);
        }*/

        public IEnumerator Run()
        {
            Debug.Log("News.Run");
            //LeanTween.moveLocalX(_items, 0f, 1f).setEase(LeanTweenType.easeOutQuint);
            //yield return new WaitForSeconds(1f);
            //yield return StartCoroutine(Build());
            yield return StartCoroutine(Build());
            //yield return null;
        }

        public IEnumerator Stop()
        {
            isRun = false;
            StopCoroutine(Slide());
            this.DestroyItems();
            Debug.Log("Destroying previous data");
            yield return new WaitForSeconds(1f);
        }

        GameObject empty;

        public void Show(bool state)
        {
            try
            {
                GameObject go = _this.transform.Find("Items").gameObject;
                for (int i = 0; i < go.transform.childCount; i++)
                {
                    go.transform.GetChild(i).gameObject.GetComponent<GUITexture>().enabled = state;
                }
            }
            catch { }
        }

        public void DestroyItems()
        {
            try
            {
                Destroy(_this.transform.Find("Items").gameObject);
            }
            catch { }
        }

        public IEnumerator Build()
        {
            if (!isRun)
            {
                isRun = true;
                this.DestroyItems();
                GameObject goNews = new GameObject();
                goNews.name = "Items";
                goNews.transform.parent = _this.transform;

                goNews.transform.localPosition = Vector3.zero;
                goNews.transform.localScale = new Vector3(1f, 1f, 1f);
                goNews.transform.localEulerAngles = Vector3.zero;

                yield return new WaitForSeconds(1);

                for (int i = 0; i < DAV.Global.mNews.Count; i++)
                {
                    if(File.Exists(DAV.Global.NEWS_PATH + "/" + DAV.Global.mNews[i]["id"] + ".png"))
                    {
                        yield return StartCoroutine(CreateItem(i));
                        //initPos += 1f;
                    }
                }
                //Debug.Log("Built");
                yield return new WaitForSeconds(1);
                StartCoroutine(Slide());
            }
            yield return new WaitForEndOfFrame();
        }

        IEnumerator CreateItem(int i)
        {
            GameObject go = new GameObject("slide[" + i + "]");
            go.transform.parent = _this.transform.Find("Items").transform;
            //go.transform.position.Set(initPos, 0.5f, 0f);
            go.transform.localPosition = new Vector3(0.5f + i, 0.5f, 0f);
            go.transform.localScale = new Vector3(1f, 1f, 1f);
            GUITexture gtex = go.AddComponent<GUITexture>();
            Texture2D tex = new Texture2D(1, 1);
            tex.LoadImage(File.ReadAllBytes(DAV.Global.NEWS_PATH + "/" + DAV.Global.mNews[i]["id"] + ".png"));
            gtex.texture = tex;
            if (DAV.Global.IsDAVPlaying)
                gtex.enabled = false;
            yield return new WaitForEndOfFrame();
        }

        IEnumerator Slide()
        {
            //this.GetComponent<AudioSource>().PlayOneShot(this.GetComponent<AudioSource>().clip);
            if (isRun)
            {
                if (_this.transform.Find("Items").transform.position.x == -(DAV.Global.mNews.Count - 1))
                    LeanTween.moveLocalX(_this.transform.Find("Items").gameObject, 1f, 1f).setEase(LeanTweenType.easeOutQuint).setDelay(Delay);
                else
                    LeanTween.moveLocalX(_this.transform.Find("Items").gameObject, _this.transform.Find("Items").transform.position.x - 1f, 1f).setEase(LeanTweenType.easeOutQuint).setDelay(Delay);

                yield return new WaitForSeconds(Delay);

                StartCoroutine(Slide());
            }
        }

        
    }
}