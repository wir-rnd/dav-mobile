﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DAV;

namespace DAV
{
    public class GUI : DAV.Behaviour
    {
        GameObject gui;
        List<string> LoadList = new List<string>();
        bool IsProgressBarShow = false;
        public bool IsRun = false;

        void Start()
        {
            gui = this.transform.Find("Canvas").gameObject;
            //this.transform.Find("Canvas/ProgressBar").gameObject.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {
            if (IsProgressBarShow)
            {
                gui.transform.Find("ProgressBar").gameObject.SetActive(true);
                IsRun = true;
                IsProgressBarShow = false;
                IsProgressDoneState = false;
                StartCoroutine(Run());
            }

            if (this.LoadList.Count > 0)
                IsProgressBarShow = true;
        }

        bool IsProgressDoneState = false;

        public IEnumerator IsProgressDone()
        {
            while (!IsProgressDoneState)
            {

            }
            yield return new WaitForEndOfFrame();
        }

        public IEnumerator Run()
        {
            if (IsRun)
            {
                gui.transform.Find("ProgressBar").Find("DownloadList").GetComponent<UnityEngine.UI.Text>().text = string.Empty;
                float total = 0f;
                int TotalLoadDone = 0;
                for (int i = 0; i < LoadList.Count; i++)
                {
                    //Debug.Log(LoadList[i]);
                    total += DAV.Global.WWWList[LoadList[i]].progress;
                    gui.transform.Find("ProgressBar").Find("DownloadList").GetComponent<UnityEngine.UI.Text>().text += LoadList[i] + "\n";
                    //Debug.Log(Utilities.WWWList[LoadList[i]].progress);DownloadList
                    if (DAV.Global.WWWList[LoadList[i]].isDone && string.IsNullOrEmpty(DAV.Global.WWWList[LoadList[i]].error))
                        TotalLoadDone++;

                    
                }
                float percentage = 0f;
                try
                {
                    percentage = (float)(total / LoadList.Count);
                }
                catch
                {
                    percentage = 1f;
                }
                //Debug.Log(total + " " + LoadList.Count + " " + percentage);

                gui.transform.Find("ProgressBar").GetComponent<UnityEngine.UI.Slider>().value = (percentage * 100);
                gui.transform.Find("ProgressBar").Find("Caption").GetComponent<UnityEngine.UI.Text>().text = ((float)(total / LoadList.Count) * 100).ToString() + "%";

                if (TotalLoadDone == LoadList.Count)
                {
                    //Debug.Log(TotalLoadDone + " " + LoadList.Count);
                    IsRun = false;
                    yield return new WaitForSeconds(2);
                    IsProgressDoneState = true;
                    gui.transform.Find("ProgressBar").GetComponent<UnityEngine.UI.Slider>().value = 0f;
                    gui.transform.Find("ProgressBar").Find("Caption").GetComponent<UnityEngine.UI.Text>().text = "0%";
                    gui.transform.Find("ProgressBar").Find("DownloadList").GetComponent<UnityEngine.UI.Text>().text = string.Empty;
                    TotalLoadDone = 0;
                    LoadList.Clear();
                    IsProgressBarShow = false;
                    gui.transform.Find("ProgressBar").gameObject.SetActive(false);
                }
            }
        }

        public void IncrementLoadList(string name)
        {
            this.LoadList.Add(name);
        }

        public void RemoveLoadList(string name)
        {
            this.LoadList.Remove(name);
        }

        public void IncrementLoadList(List<string> list)
        {
            for (int i = 0; i < list.Count; i++)
                this.LoadList.Add(list[i]);
        }

        /*public IEnumerator AvailableProgressCheck()
        {
            if(!IsRun)
                yield return new 
        }*/
    }
}