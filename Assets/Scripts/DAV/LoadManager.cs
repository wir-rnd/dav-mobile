﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using SimpleJSON;
using DAV;

namespace DAV
{
    public class LoadManager : DAV.Behaviour
    {

        public static int JSONTargetState = 0;
        public static int JSONContentByDeviceState = 0;
        public static int JSONContentByTargetState = 0;
        //public static int JSONContentCloudState = 0;
        public static int VideoTextureState = 0;



        public static JSONNode mTarget;
        public static JSONNode mCTarget;
        public static JSONNode mCDevice;

        public JSONNode JSON { get; set; }

        public IEnumerator Run(string URL, WWWForm parameter, string keyState, WWWType type, string storeFilePath)
        {

            if (DAV.Global.State[keyState] == StateType.IDLE)
            {
                
                if (parameter == null)
                {

                    if (!DAV.Global.WWWList.ContainsKey(keyState))
                    {
                        
                        DAV.Global.WWWList.Add(keyState, new WWW(URL));
                    }
                    else
                        DAV.Global.WWWList[keyState] = new WWW(URL);
                }
                else
                {
                    if (!DAV.Global.WWWList.ContainsKey(keyState))
                        DAV.Global.WWWList.Add(keyState, new WWW(URL, parameter));
                    else
                        DAV.Global.WWWList[keyState] = new WWW(URL, parameter);
                }


                yield return DAV.Global.WWWList[keyState];

 

                if (string.IsNullOrEmpty(DAV.Global.WWWList[keyState].error))
                {
                    DAV.Global.State[keyState] = StateType.LOAD_SUCCESS;
                    //Debug.Log(Utilities.WWWList[keyState].text);
                    if (type == WWWType.JSON)
                    {
                        JSON = new JSONNode();
                        if (Utilities.IsJson(DAV.Global.WWWList[keyState].text))
                        {
                            JSON = JSONNode.Parse(DAV.Global.WWWList[keyState].text);
                            
                            DAV.Global.State[keyState] = StateType.PARSE_SUCCESS;
                        }
                        else
                        {
                            DAV.Global.State[keyState] = StateType.PARSE_FAILED;
                            Debug.Log("Parse Failed");
                        }
                    }
                    else if (type == WWWType.FILE)
                    {
                        File.WriteAllBytes(storeFilePath, DAV.Global.WWWList[keyState].bytes);
                        DAV.Global.State[keyState] = StateType.FILE_STORED;
                    }
                    //LoadState = 1;
                }
                else
                {
                    DAV.Global.State[keyState] = StateType.LOAD_FAILED;
                    Debug.Log(keyState + "::" + DAV.Global.WWWList[keyState].error);
                    //DAV.Global.StateReset(keyState);
                    //LoadState = 2;
                }
            }
        }

        /*
        public static IEnumerator JSONTarget()
        {
            if (JSONTargetState == 0)
            {
                WWWForm wf = new WWWForm();
                wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                WWW download = new WWW(DownloadManager.WEB_SERVICE_URL + @"dws/target", wf);
                DAV.Main.downloadList.Add(download);
                Debug.Log(SystemInfo.deviceUniqueIdentifier + " " + DownloadManager.WEB_SERVICE_URL + @"dws/target");
                yield return download;
                if (string.IsNullOrEmpty(download.error))
                {
                    Debug.Log(download.text);
                    //DownloadManager.mTarget = JsonConvert.DeserializeObject<jTarget>(download.text);
                    DownloadManager.mTarget = SimpleJSON.JSONNode.Parse(download.text);
                    JSONTargetState = 1;
                }
                else
                {
                    Debug.Log(download.error);
                    JSONTargetState = 2;
                }
            }
        }
    
        public static IEnumerator JSONContentByTarget(string id)
        {
            if (JSONContentByTargetState == 0)
            {
                WWWForm wf = new WWWForm();
                wf.AddField("target_id", id);
                wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                wf.AddField("bytype", "ByTargetID");
                WWW download = new WWW( DownloadManager.WEB_SERVICE_URL+@"dws/content", wf );
                DAV.Main.downloadList.Add(download);
                yield return download;
                if(string.IsNullOrEmpty(download.error))
                {
                    Debug.Log (download.text);
                    DownloadManager.mCTarget = JsonConvert.DeserializeObject<jContentByTarget>(download.text);
                    JSONContentByTargetState = 1;
                }
                else
                {
                    Debug.Log (download.error);
                    JSONContentByTargetState = 2;
                }
            }
        }

        public static IEnumerator JSONContentByDevice()
        {
            if (JSONContentByDeviceState == 0)
            {
                WWWForm wf = new WWWForm();
                wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                wf.AddField("bytype", "BySN");
                WWW download = new WWW(DownloadManager.WEB_SERVICE_URL + @"dws/content", wf);
                DAV.Main.downloadList.Add(download);
                yield return download;

                if (string.IsNullOrEmpty(download.error))
                {
                    Debug.Log(download.text);
                    try
                    {
                        DownloadManager.mCDevice = JsonConvert.DeserializeObject<jContentByDevice>(download.text);
                        JSONContentByDeviceState = 1;
                    }
                    catch
                    {
                        JSONContentByDeviceState = 3;
                    }
                
                
                }
                else
                {
                    Debug.Log(download.error);
                    JSONContentByDeviceState = 2;
                }
            }
        }


        public static IEnumerator SendStatistic(string target)
        {
            if (SendStatisticState == 0)
            {
                string serial = SystemInfo.deviceUniqueIdentifier.ToString();
                WWWForm wf = new WWWForm();
                wf.AddField("serial_number", serial);
                wf.AddField("target_name", target);
                var download = new WWW(DownloadManager.WEB_SERVICE_URL + @"home/sendStatistic", wf);
                yield return download;

                if (string.IsNullOrEmpty(download.error))
                {
                    Debug.Log("SEND STATISTIC " + target + " " + serial);
                    //Debug.Log (download.text);
                    //DownloadManager.mTarget = JsonConvert.DeserializeObject<jTarget>(download.text);
                    SendStatisticState = 1;
                }
                else
                {
                    Debug.Log(download.error);
                    SendStatisticState = 2;
                }
            }
        }

        public static IEnumerator Ping()
        {
            if (PING == 0)
            {
                string serial = SystemInfo.deviceUniqueIdentifier.ToString();
                WWWForm wf = new WWWForm();
                wf.AddField("serial_number", serial);

                var download = new WWW(DownloadManager.WEB_SERVICE_URL + @"home/ping", wf);
                yield return download;

                if (string.IsNullOrEmpty(download.error)) PING = 1;
                else
                {
                    Debug.Log(download.error);
                    PING = 2;
                }
            }
        }

        public static IEnumerator Target(int DTIdx, int TargetIdx, string type)
        {
            jTargetTracking currentTarget = DownloadManager.mTarget.Tracking[TargetIdx];

            string path = DownloadManager.TARGETS_PATH + "/" + currentTarget.Name + type;
            DAV.Main.DT[DTIdx].Download = new WWW(DownloadManager.WEB_SERVICE_URL + "targets/" + currentTarget.Name + "/" + currentTarget.Name + type);
            //Debug.Log("DL URL : " + DownloadManager.WEB_SERVICE_URL + "targets/" + currentTracking.Name + "/" + currentTracking.Name + type);
            yield return DAV.Main.DT[DTIdx].Download;
            try
            {
                if (DAV.Main.DT[DTIdx].Download.progress >= 1)
                {
                    if (string.IsNullOrEmpty(DAV.Main.DT[DTIdx].Download.error))
                    {
                        try
                        {
                            //Debug.Log("write " + path);
                            File.WriteAllBytes(path, DAV.Main.DT[DTIdx].Download.bytes);
                        }
                        catch
                        {
                            //msg.Show("File Write", "File " + path + "/" + currentContent.ContentDetail[contentDetailIndex].Name + " failed to write", true, 0);
                            Debug.Log("error write " + path);
                        }
                    }
                    //Debug.Log(" Download State of Content ID " + Main.DC[index].ContentID + " & Detail Content ID " + Main.DC[index].ContentDetailID + " is " + Main.DC[index].DownloadState);
                }
            }
            catch(Exception E)
            {
                Debug.Log("download failed : " + E);
            }
        }

        public static int DOWNLOAD_CONTENT_TYPE = 0;
        public static IEnumerator Content(int index)
        {
            Message msg = (Message)MonoBehaviour.FindObjectOfType(typeof(Message));
            jContent currentContent = DownloadManager.mCDevice.Contents[DAV.Main.DC[index].ContentID];

            string path = DownloadManager.CONTENTS_PATH + "/" + currentContent.Name;
            DAV.Main.DC[index].Download = new WWW(DownloadManager.WEB_SERVICE_URL + "contents/" + currentContent.Name + "/" + currentContent.ContentDetail[DAV.Main.DC[index].ContentDetailID].Name);
            yield return DAV.Main.DC[index].Download;
            try
            {
                if (DAV.Main.DC[index].Download.progress >= 1)
                {
                    if (string.IsNullOrEmpty(DAV.Main.DC[index].Download.error))
                    {
                        try
                        {
                            File.WriteAllBytes(path + "/" + currentContent.ContentDetail[DAV.Main.DC[index].ContentDetailID].Name, DAV.Main.DC[index].Download.bytes);
                        }
                        catch
                        {
                            //msg.Show("File Write", "File " + path + "/" + currentContent.ContentDetail[contentDetailIndex].Name + " failed to write", true, 0);
                            Debug.Log("error write " + path + "/" + currentContent.ContentDetail[DAV.Main.DC[index].ContentDetailID].Name);
                        }
                    }
                    //Debug.Log(" Download State of Content ID " + Main.DC[index].ContentID + " & Detail Content ID " + Main.DC[index].ContentDetailID + " is " + Main.DC[index].DownloadState);
                }
            }
            catch
            {
                Debug.Log("download failed for " + path);
            }
        }*/
    }
}