﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace DAV
{
    public class Main : DAV.Behaviour
    {
        float TIME_RELOAD_APT = 40f;
        float TIME_RELOAD_APT_EXEC = 0f;
        float TIME_RELOAD_APN = 3600f;/*28800f;*/
        float TIME_RELOAD_APN_EXEC = 60f;
        float TIME_RELOAD_APC = 20f;
        float TIME_RELOAD_APC_EXEC = 0f;
        float TIME_RELOAD_PING = 3600f;


        void Start()
        {
            Debug.Log(SystemInfo.deviceUniqueIdentifier);

            //this.davMessage.Show("Welcome", "DAV", true, 0);

            Instance.CheckLocalDirectory();
            StartCoroutine(init());
            //StartCoroutine(AA());

            //StartCoroutine(AutoPushDAVNews());

            //StartCoroutine(AutoPushTracking());

            //StartCoroutine(AutoPing());
        }

        IEnumerator init()
        {
            yield return new WaitForSeconds(1);
            yield return StartCoroutine(InitConfig());
            StartCoroutine(AutoPushDAVNews());
            StartCoroutine(AutoPushTracking());
            //StartCoroutine(AutoPushContent());
        }

        IEnumerator InitConfig()
        {
            string name = System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name;
            name = Utilities.CleanMethodAssemblyName(name);
            DAV.Global.StateCheck(name);
            DAV.Global.StateReset(name);
            
            yield return StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + @"dws/config", null, name, WWWType.JSON, string.Empty));
            switch (DAV.Global.State[name])
            {
                case StateType.LOAD_FAILED:
                    this.davMessage.Show("DAV News", "Load last config..", false, 2);
                    DAV.Global.mConfig = SimpleJSON.JSONNode.LoadFromFile(DAV.Global.OFFLINE_DAT_PATH + "/config.dat");

                    break;
                case StateType.PARSE_SUCCESS:
                    DAV.Global.mConfig = loadMgr.JSON;
                    DAV.Global.mConfig.SaveToFile(DAV.Global.OFFLINE_DAT_PATH + "/config.dat");

                    for (int i = 0; i < DAV.Global.mConfig.Count; i++)
                    {
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APT"))
                            TIME_RELOAD_APT = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if(DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APT_EXEC"))
                            TIME_RELOAD_APT_EXEC = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APN"))
                            TIME_RELOAD_APN = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APN_EXEC"))
                            TIME_RELOAD_APN_EXEC = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APC"))
                            TIME_RELOAD_APC = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_APC_EXEC"))
                            TIME_RELOAD_APC_EXEC = float.Parse(DAV.Global.mConfig[i]["value"]);
                        if (DAV.Global.mConfig[i]["name"].Equals("TIME_RELOAD_PING"))
                            TIME_RELOAD_PING = float.Parse(DAV.Global.mConfig[i]["value"]);
                    }

                    break;
            }
            yield return new WaitForSeconds(1f);
        }

        IEnumerator ExecDAVNews()
        {
            yield return StartCoroutine(this.davNews.Stop());
            StartCoroutine(this.davNews.Run());
            yield return new WaitForSeconds(TIME_RELOAD_APN_EXEC);
            StartCoroutine(ExecDAVNews());
        }

        IEnumerator AutoPushDAVNews()
        {
            if (!DAV.Global.IsDAVPlaying)
            {
                StopCoroutine("ExecDAVNews");
                Instance.CheckLocalDirectory();
                string name = System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name;
                name = Utilities.CleanMethodAssemblyName(name);
                DAV.Global.StateCheck(name);
                DAV.Global.StateReset(name);
                yield return StartCoroutine(this.davNews.Stop());

                yield return StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + @"dws/news", null, name, WWWType.JSON, string.Empty));
                //this.davMessage.Show("State", DAVGlobal.State[name].ToString(), true, 0);
                switch (DAV.Global.State[name])
                {
                    case StateType.LOAD_SUCCESS:
                        //this.davMessage.Show("asdasd", "aaaa" + DAVGlobal.State[name].ToString(), true, 0);
                        break;
                    case StateType.LOAD_FAILED:
                        this.davMessage.SimpleShow("DAV News fail to Load, load last news...");
                        if (File.Exists(DAV.Global.OFFLINE_DAT_PATH + "/news.dat"))
                        {
                            DAV.Global.mNews = SimpleJSON.JSONNode.LoadFromFile(DAV.Global.OFFLINE_DAT_PATH + "/news.dat");
                            for (int i = 0; i < DAV.Global.mNews.Count; i++)
                            {
                                if (!File.Exists(DAV.Global.NEWS_PATH + "/" + DAV.Global.mNews[i]["id"] + ".png"))
                                {
                                    StartCoroutine(AutoPushDAVNews());
                                    break;
                                }
                            }
                        }

                        //StartCoroutine(this.davNews.Run());
                        this.davNews.Run();
                        break;
                    case StateType.PARSE_SUCCESS:
                        DAV.Global.mNews = loadMgr.JSON;
                        DAV.Global.mNews.SaveToFile(DAV.Global.OFFLINE_DAT_PATH + "/news.dat");

                        

                        string id = string.Empty;
                        string path = string.Empty;
                        List<string> list = new List<string>();
                        for (int i = 0; i < DAV.Global.mNews.Count; i++)
                        {
                            id = "news-" + i + "-" + DAV.Global.mNews[i]["id"];
                            //Debug.Log(id);
                            DAV.Global.StateCheck(id);
                            DAV.Global.StateReset(id);

                            path = DAV.Global.WEB_SERVICE_URL + "assets/images/news/" + DAV.Global.mNews[i]["id"] + ".png";
                            if (!Utilities.FileCheck(DAV.Global.NEWS_PATH + "/" + DAV.Global.mNews[i]["id"] + ".png", DAV.Global.mNews[i]["md5"]))
                            {
                                //try { File.Delete(DAVGlobal.NEWS_PATH + "/" + DAVGlobal.mNews[i]["id"] + ".jpg"); }
                                //catch { }
                                //this.davMessage.Show("asdasd", DAVGlobal.NEWS_PATH + "/" + DAVGlobal.mNews[i]["id"] + ".jpg", true, 0);
                                StartCoroutine(loadMgr.Run(path, null, id, WWWType.FILE, DAV.Global.NEWS_PATH + "/" + DAV.Global.mNews[i]["id"] + ".png"));
                                list.Add(id);
                                davGUI.IncrementLoadList(id);
                            }
                        }
                        //davGUI.IncrementLoadList(list);
                        yield return StartCoroutine(Instance.IsDownloadDone(list, name));
                        StartCoroutine(ExecDAVNews());
                        break;
                    case StateType.PARSE_FAILED:
                        this.davMessage.Show("Parse Failed", "Parse Failed to get DAV News", true);
                        break;
                }
            }
            yield return new WaitForSeconds(TIME_RELOAD_APN);
            StartCoroutine(AutoPushDAVNews());
        }

        IEnumerator ExecTracking()
        {
            yield return StartCoroutine(this.davTracking.Run());
            
            //StartCoroutine(ExecContent());
            if (TIME_RELOAD_APT_EXEC > 0)
            {
                yield return new WaitForSeconds(TIME_RELOAD_APT_EXEC);
                StartCoroutine(ExecTracking());
            }
        }

        IEnumerator AutoPushTracking()
        {
            if (!DAV.Global.IsDAVPlaying)
            {
                Instance.CheckLocalDirectory();
                string name = System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name;
                name = Utilities.CleanMethodAssemblyName(name);
                DAV.Global.StateCheck(name);
                DAV.Global.StateReset(name);
                Debug.Log(name);

                WWWForm wf = new WWWForm();

                wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                yield return StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + @"dws/target", wf, name, WWWType.JSON, string.Empty));

                switch (DAV.Global.State[name])
                {
                    case StateType.LOAD_SUCCESS:
                        Debug.Log(loadMgr.JSON.ToString());
                        break;
                    case StateType.LOAD_FAILED:
                        this.davMessage.SimpleShow("DAV Tracking fail to Load, load last tracking...");
                        DAV.Global.mTrackings = SimpleJSON.JSONNode.LoadFromFile(DAV.Global.OFFLINE_DAT_PATH + "/trackings.dat");
                        StartCoroutine(ExecTracking());
                        break;
                    case StateType.PARSE_SUCCESS:
                        DAV.Global.mTrackings = loadMgr.JSON;
                        DAV.Global.mTrackings.SaveToFile(DAV.Global.OFFLINE_DAT_PATH + "/trackings.dat");
                        Debug.Log(DAV.Global.WWWList[name].text);
                        yield return StartCoroutine(this.davTracking.InitDownload(name));
                        yield return StartCoroutine(davTracking.Run());
                        StartCoroutine(AutoPushContent());
                        break;
                    case StateType.PARSE_FAILED:
                        Debug.Log("asdasdasd");
                        break;
                }
            }
            yield return new WaitForSeconds(TIME_RELOAD_APT);
            StartCoroutine(AutoPushTracking());
        }

        IEnumerator ExecContent()
        {
            yield return StartCoroutine(this.davContent.Run());
            if (TIME_RELOAD_APC_EXEC > 0)
            {
                yield return new WaitForSeconds(TIME_RELOAD_APC_EXEC);
                StartCoroutine(ExecContent());
            }
        }

        IEnumerator Exec()
        {
            yield return StartCoroutine(this.davTracking.Run());
            yield return StartCoroutine(this.davContent.Run());
            yield return new WaitForSeconds(10);
            StartCoroutine(Exec());
        }

        IEnumerator AutoPushContent()
        {
            if (!DAV.Global.IsDAVPlaying)
            {
                this.davMessage.SimpleShow("AutoPushContent");
                Instance.CheckLocalDirectory();
                string name = System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name;
                name = Utilities.CleanMethodAssemblyName(name);
                Debug.Log(name);
                DAV.Global.StateCheck(name);
                DAV.Global.StateReset(name);
                Debug.Log(name);
                WWWForm wf = new WWWForm();

                wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                wf.AddField("bytype", "BySN");
                yield return StartCoroutine(loadMgr.Run(DAV.Global.WEB_SERVICE_URL + @"dws/content", wf, name, WWWType.JSON, string.Empty));
                //Debug.Log(DAV.Global.WWWList[name].text);
                switch (DAV.Global.State[name])
                {
                    case StateType.LOAD_SUCCESS:
                        this.davMessage.SimpleShow("DAV Content success to load");
                        Debug.Log(loadMgr.JSON.ToString());
                        break;
                    case StateType.LOAD_FAILED:
                        this.davMessage.SimpleShow("DAV Content fail to Load, load last content...");
                        DAV.Global.mContents = SimpleJSON.JSONNode.LoadFromFile(DAV.Global.OFFLINE_DAT_PATH + "/contents.dat");
                        //StartCoroutine(ExecTracking());
                        break;
                    case StateType.PARSE_SUCCESS:
                        this.davMessage.SimpleShow("DAV Content success to parse");
                        DAV.Global.mContents = loadMgr.JSON;
                        DAV.Global.mContents.SaveToFile(DAV.Global.OFFLINE_DAT_PATH + "/contents.dat");
                        yield return StartCoroutine(this.davContent.InitDownload(name));
                        yield return StartCoroutine(this.davContent.Run());
                        //yield return new WaitForSeconds(1);
                        //StopCoroutine(Exec());
                        //StartCoroutine(Exec());
                        break;
                    case StateType.PARSE_FAILED:
                        Debug.Log("asdasdasd");
                        break;
                }
            }
            //yield return new WaitForSeconds(TIME_RELOAD_APC);
            //StartCoroutine(AutoPushContent());
        }


        IEnumerator ReloadContent() // Confirm if need to re-download and build content to be object scene
        {
            //if (AllContentLoaded)
            {
                //mObjBuilder.UnActiveAllObjects();
                //Main.debugMsg.text = "Content3DState - " + DownloadManager.Content3DState.ToString();

                //uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(false);
                /*for (int i = 0; i < DownloadManager.mCDevice.Contents.Count;i++)
                {
                    currentContent = DownloadManager.mCDevice.Contents[i];
                    for (int j = 0; j < DownloadManager.mCDevice.Contents[i].ContentDetail.Count; j++)
                    {
                        path = DownloadManager.CONTENTS_PATH + "/" + currentContent.Name;
                        if (!File.Exists(path))
                        {
                            Debug.Log("One of Content doesn't exist");
                            AllContentLoaded = false;
                            contentDLstate.Clear();
                            DC.Clear();
                            yield return new WaitForSeconds(1);
                            StartCoroutine(AutoPushContent());
                            yield break;
                        }
                    }
                }*/
                Debug.Log("all data content pass");

                /*for (int i = 0; i < DownloadManager.mCDevice.Contents.Count; i++)
                {
                    currentContent = DownloadManager.mCDevice.Contents[i];
                    for (int j = 0; j < currentContent.ContentDetail.Count; j++)
                    {
                        path = DownloadManager.CONTENTS_PATH + "/" + currentContent.Name;
                        string key = currentContent.Name + "_" + currentContent.ContentDetail[j].Name;
                        //Debug.Log("ContentDetail :: " + currentContent.ContentDetail[j].Name);
                        mObjBuilder.SetRootObject(mTargetsObject.transform.Find(currentContent.TargetName).gameObject);
                        if (currentContent.ContentDetail[j].Name.Split('.')[1].Equals("unity3d"))
                        {
                            Debug.Log("SetRootObject :: " + currentContent.TargetName);

                            if (!DictAssetBundle.ContainsKey(key))
                            {
                                //Debug.Log("1:" + currentContent.ContentDetail[j].Name);
                                mObjBuilder.SetName(currentContent.Name);
                                DownloadManager.mAssetBundle = AssetBundle.CreateFromFile(path + "/" + currentContent.ContentDetail[j].Name); Debug.Log("DictAssetBundle no Contains " + currentContent.Name + "_" + currentContent.ContentDetail[j].Name);

                                mObjBuilder.BuildObject(DownloadManager.mAssetBundle);
                                mObjBuilder.AssemblyObjectToTarget(currentContent.Name, currentContent.TargetName);

                                //mObjBuilder.SetActiveObject(false);
                                mObjBuilder.SetPosition(currentContent.px, currentContent.py, currentContent.pz);
                                mObjBuilder.SetRotation(currentContent.rx, currentContent.ry, currentContent.rz);
                                mObjBuilder.SetScale(currentContent.sx, currentContent.sy, currentContent.sz);
                                //GameObject.Find("Function/AB").GetComponent<AB>().SetAB(DownloadManager.mAssetBundle);



                                DictAssetBundle.Add(key, mObjBuilder.GetObject3D());
                            }
                            else
                            {
                                Debug.Log("DictAssetBundle Contains " + key);
                                mObjBuilder.AssemblyObjectToTarget(currentContent.Name, currentContent.TargetName);

                                //mObjBuilder.SetActiveObject(false);
                                mObjBuilder.SetPosition(currentContent.px, currentContent.py, currentContent.pz);
                                mObjBuilder.SetRotation(currentContent.rx, currentContent.ry, currentContent.rz);
                                mObjBuilder.SetScale(currentContent.sx, currentContent.sy, currentContent.sz);
                            }
                        }
                        else if (currentContent.ContentDetail[j].Name.Split('.')[1].Equals("dll"))
                        {
                            //Debug.Log("2:" + currentContent.ContentDetail[j].Name);
                            //Debug.Log("Path : " + path + "/" + currentContent.ContentDetail[j].Name);
                            byte[] b = File.ReadAllBytes(path + "/" + currentContent.ContentDetail[j].Name);
                            if (!DictAssetBundle.ContainsKey(key))
                            {
                                //Debug.Log("DictAssetBundle no Contains " + currentContent.Name + "_" + currentContent.ContentDetail[j].Name);
                                mObjBuilder.AssemblySet = true;
                                mObjBuilder.BuildAssembly(b);
                                mObjBuilder.LoadAssembly();
                                DictAssetBundle.Add(key, b);

                            }
                            else
                            {
                                Debug.Log("DictAssetBundle Contains " + key);
                                //mObjBuilder.UnloadAssembly();
                                mObjBuilder.AssemblySet = true;
                                mObjBuilder.BuildAssembly(b);
                                mObjBuilder.LoadAssembly();
                            }
                        }
                        else if (currentContent.ContentDetail[j].Name.Split('.')[1].Equals("mp4"))
                        {
                            if (!mIsVideoLoaded)
                            {
                                //StartCoroutine(this.VideoPlayer(path + "/" + currentContent.ContentDetail[i].Name));
                                VideoObject.transform.localScale = new Vector3(0.1f, 0.04999999f, 0.04999999f);
                                mIsVideoLoaded = true;
                            }
                        }
                        else
                        {
                            //Debug.Log("3:" + currentContent.ContentDetail[j].Name);
                            if (!DictAssetBundle.ContainsKey(key))
                            {
                                //Debug.Log("DictAssetBundle no Contains " + currentContent.Name + "_" + currentContent.ContentDetail[j].Name);
                                DictAssetBundle.Add(key, key.Split('.')[1]);
                            }
                            else
                            {
                                //Debug.Log("DictAssetBundle Contains " + key);
                            }
                        }

                        mIsLoadingContent = false;
                        DownloadManager.JSONContentByDeviceState = 0;
                    }
                }*/
            }
            yield return null;
        }

        /*

        public void TargetCreated(string targetId)
        {
            mLastTargetId = targetId;
            mIsLoadingTarget = true;
            mIsTargetRequested = false;
            TotalContents = 0;
            LoadTarget(targetId);
        }

        public void TargetDeleted()
        {
            mIsLoadingTarget = false;
            mIsLoadingContent = false;
            mIsTargetRequested = false;
            mIsContentRequested = false;
            mIsSwitchContent = false;
            mIsSendStatistic = false;
            mIsVideoLoaded = false;



            this.IndexContent = 0;
            this.TotalContents = 0;

		
            DownloadManager.mTarget = null;
            downloadList.Clear();

            coroutineState_Target = false;
            coroutineState_Content = false;
            coroutineState_Content3D = false;
            coroutineState_SendStatistic = false;
            coroutineState_AllDownloadParticular = false;
            CheckAllFinishedDownloadParticularState = true;

            uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(false);



            if (DC != null)
            {
                for (int i = 0; i < DC.Count; i++)
                {
                    //Debug.Log("StopCoroutine " + this.IndexContent + " - " + IndexDownloadParticular[i]);
                    if (!string.IsNullOrEmpty(DC[i].Download.error))
                        DC[i].Download.Dispose();

                    StopCoroutine(DownloadManager.Content(i));
                
                }
                for(int i=0;i<DC.Count;i++)
                {
                    if (DC[i].Download == null)
                        Destroy(uiRoot.transform.Find("Progress/ProgressBar_" + i).gameObject);
                }
            }


            DownloadManager.JSONTargetCloudState = 0;
            DownloadManager.SendStatisticState = 0;

            try
            {
                mObjBuilder.SetActiveObject(false);
                VideoObject.transform.localScale = new Vector3(0f, 0f, 0f);
                currentVideo.VideoPlayer.Stop();
                //video.VideoPlayer.Unload();
                //video.VideoPlayer.Deinit();
            }
            catch
            {
                VideoObject.transform.localScale = new Vector3(0f, 0f, 0f);
            }

            //StartCoroutine(ReloadCloudEngine());
        }*/

        
        /*private IEnumerator LoadTarget()
        {
            string name = System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name;
            Utilities.StateCheck(name);
            Utilities.StateReset(name);

            

            //LoadManager.LoadStateReset(name);
        }*/

        /*private IEnumerator LoadTarget1()
        {
            LoadManager.LoadState = 0;
            WWWForm wf = new WWWForm();
            if (!mIsTargetRequested)
            {
                if (!coroutineState_Target)
                {
                    wf.AddField("sn", SystemInfo.deviceUniqueIdentifier);
                    StartCoroutine(LoadManager.Down(@"dws/target", wf));
                    coroutineState_Target = true;
                }

                if (LoadManager.LoadState != 0)
                {
                    Debug.Log("StopCoroutine(JSONTarget)");
                    StopCoroutine("DownloadManager.JSONTarget()");
                }

                switch (LoadManager.LoadState)
                {
                    case 1:

                        LoadManager.LoadState = 0;

                        this.TotalTargets = LoadManager.JSON["Tracking"].Count;
                        Debug.Log(this.TotalTargets);
                        //if (DownloadManager.mTarget.Tracking != null)
                        //    this.TotalTargets = DownloadManager.mTarget.Tracking.Count;

                        string path = string.Empty;


                        DT = new List<DataTarget>();
                        for (int i = 0; i < this.TotalTargets; i++)
                        {
                            path = DownloadManager.TARGETS_PATH + "/" + DownloadManager.mTarget.Tracking[i].Name + DownloadManager.DAT_TYPE;

                            if (File.Exists(path))
                            {
                                Debug.Log(Utilities.GetMD5Checksum(path) + " - " + DownloadManager.mTarget.Tracking[i].DAT_MD5 + " :: " + Utilities.GetMD5Checksum(path).Equals(DownloadManager.mTarget.Tracking[i].DAT_MD5));
                                if (!Utilities.GetMD5Checksum(path).Equals(DownloadManager.mTarget.Tracking[i].DAT_MD5))
                                    DT.Add(new DataTarget() { Index = i, TargetID = DownloadManager.mTarget.Tracking[i].TargetID, Type = DownloadManager.DAT_TYPE, Download = null });
                            }
                            else
                            {
                                DT.Add(new DataTarget() { Index = i, TargetID = DownloadManager.mTarget.Tracking[i].TargetID, Type = DownloadManager.DAT_TYPE, Download = null });
                            }

                            path = DownloadManager.TARGETS_PATH + "/" + DownloadManager.mTarget.Tracking[i].Name + DownloadManager.XML_TYPE;

                            if (File.Exists(path))
                            {
                                Debug.Log(Utilities.GetMD5Checksum(path) + " - " + DownloadManager.mTarget.Tracking[i].XML_MD5 + " :: " + Utilities.GetMD5Checksum(path).Equals(DownloadManager.mTarget.Tracking[i].XML_MD5));
                                if (!Utilities.GetMD5Checksum(path).Equals(DownloadManager.mTarget.Tracking[i].XML_MD5))
                                    DT.Add(new DataTarget() { Index = i, TargetID = DownloadManager.mTarget.Tracking[i].TargetID, Type = DownloadManager.XML_TYPE, Download = null });
                            }
                            else
                            {
                                DT.Add(new DataTarget() { Index = i, TargetID = DownloadManager.mTarget.Tracking[i].TargetID, Type = DownloadManager.XML_TYPE, Download = null });
                            }
                        }

                        //Debug.Log(DT.Count);
                        if (DT.Count > 0)
                            StartCoroutine(DownloadAllTargets());
                        else
                        {
                            coroutineState_Target = true;
                            AllTrackingLoaded = true;
                        }

                        //uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(false);
                        mIsTargetRequested = true;
                        break;
                    case 2:
                        mIsTargetRequested = true;
                        yield return null;
                        break;
                }
            }
        }*/

        /*
        private IEnumerator LoadContentByDevice()
        {
            //Debug.Log("LoadContentByDevice");
            if (!mIsContentByDeviceRequested)
            {
                if (!coroutineState_ContentByDevice)
                {
                    StartCoroutine(DownloadManager.JSONContentByDevice());
                    coroutineState_ContentByDevice = true;
                }

                if (DownloadManager.JSONContentByDeviceState != 0)
                {
                    Debug.Log("StopCoroutine(LoadContentByDevice)");
                    StopCoroutine("DownloadManager.LoadContentByDevice()");
                }

                switch (DownloadManager.JSONContentByDeviceState)
                {
                    case 1:

                        mIsContentByDeviceRequested = true;

                        if (DownloadManager.mCDevice.Contents != null)
                            this.TotalContents = DownloadManager.mCDevice.Contents.Count;
                        else
                            this.TotalContents = 0;

                        this.IndexContent = 0;
                        string path = string.Empty;

                        DownloadManager.DOWNLOAD_CONTENT_TYPE = 1;
                        DC = new List<DataContent>();

                        for (int i = 0; i < this.TotalContents; i++)
                        {
                            path = DownloadManager.CONTENTS_PATH + "/" + DownloadManager.mCDevice.Contents[i].Name;
                            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                            for (int j = 0; j < DownloadManager.mCDevice.Contents[i].ContentDetail.Count; j++)
                            {
                                path = DownloadManager.CONTENTS_PATH + "/" + DownloadManager.mCDevice.Contents[i].Name + "/" + DownloadManager.mCDevice.Contents[i].ContentDetail[j].Name;
                                if (File.Exists(path))
                                {
                                    //Debug.Log("Exists at " + path + " for Content '" + DownloadManager.mDevice.Contents[i].Name + "'");
                                    if (!Utilities.GetMD5Checksum(path).Equals(DownloadManager.mCDevice.Contents[i].ContentDetail[j].MD5Checksum))
                                    {
                                        DC.Add(new DataContent() { ContentID = i, ContentDetailID = j, Download = null });
                                    }
                                }
                                else
                                {
                                    DC.Add(new DataContent() { ContentID = i, ContentDetailID = j, Download = null });
                                }
                            }
                        }

                        //uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(true);

                        if (DC.Count > 0)
                            StartCoroutine(DownloadAllContent());
                        else
                        {
                            coroutineState_ContentByDevice = true;
                            AllContentLoaded = true;
                        }

                        //yield return StartCoroutine(DownloadAllContent());

                        DownloadManager.JSONContentByDeviceState = 0;

                        break;
                    case 2:
                        msg.Show("Connection Error", "Please check your internet connection", true, 0);
                        mIsContentByDeviceRequested = true;
                        break;
                    case 3:
                        msg.Show("No Assignment", "No one store assign this device or this devices didn't registered yet, please check at DAV Control center. Your Device Number is " + SystemInfo.deviceUniqueIdentifier.ToString(), true, 0);
                        mIsContentByDeviceRequested = true;
                        break;
                }
            }
            yield return null;
        }

        private void LoadContentByTarget(string targetId)
        {
            if (!mIsContentByTargetRequested)
            {
                if (!coroutineState_ContentByTarget)
                {
                    StartCoroutine(DownloadManager.JSONContentByTarget(targetId));
                    coroutineState_ContentByTarget = true;
                }

                if (DownloadManager.JSONContentByTargetState != 0)
                {
                    Debug.Log("StopCoroutine(JSONTargetCloud)");
                    StopCoroutine(DownloadManager.JSONContentByTarget(targetId));
                }

                switch (DownloadManager.JSONContentByTargetState)
                {
                    case 1:
                        if (DownloadManager.mCTarget.Contents != null)
                            this.TotalContents = DownloadManager.mCTarget.Contents.Count;
                        else
                            this.TotalContents = 0;
                        this.IndexContent = 0;

                        mIsContentByTargetRequested = true;
                        mIsLoadingTarget = false;
                        mIsLoadingContent = true;
                        Debug.Log(this.TotalContents + " " + this.IndexContent);
                        SendStatistic(targetId);

                        if (TotalContents == 0)
                            msg.Show("No Assignment", "This Device has no Content or Marker Assigment, Please check at DAV Control Center", true, 0);

                        break;
                    case 2:
                        //TargetDeleted();

                        //Debug.Log("Connection Error", "Target Request Failed. Please Check your Internet Connection", LoadTargetError);
                        Debug.Log("Target Request Failed. Please Check your Internet Connection");
                        break;
                }
            }
        }

        private void SendStatistic(string targetId)
        {
            if (!mIsSendStatistic)
            {
                if (!coroutineState_SendStatistic)
                {
                    StartCoroutine(DownloadManager.SendStatistic(targetId));
                    coroutineState_SendStatistic = true;
                    Debug.Log("StartCoroutine(SendStatistic)");
                }
                if (DownloadManager.SendStatisticState != 0)
                {
                    Debug.Log("StopCoroutine(SendStatistic)");
                    StopCoroutine("SendStatistic");
                }

                switch (DownloadManager.SendStatisticState)
                {
                    case 1:
                        Debug.Log("Statistic Sent for " + targetId);
                        mIsSendStatistic = true;
                        break;
                    case 2:
                        mIsSendStatistic = true;
                        //Debug.Log("Connection Error", "Target Request Failed. Please Check your Internet Connection", LoadTargetError);
                        Debug.Log("Send statistic Failed. Please Check your Internet Connection");
                        break;
                }
            }
        }*/



        /*private void LoadContent()
        {
            if(!mIsContentRequested)
            {
                Debug.Log("mIsContentRequested");
                //Debug.Log ("mIsTargetContentRequested123 "+Cor2state);
                if(!coroutineState_Content)
                {
                    Debug.Log("coroutineState_Content " + this.IndexContent + " " + DownloadManager.mTarget.Contents[this.IndexContent].Name);
                    StartCoroutine(DownloadManager.JSONContentCloud());
                    coroutineState_Content = true;
                }
                if(DownloadManager.JSONContentCloudState!=0)
                {
                    Debug.Log ("StopCoroutine(JSONContentCloud)");
                    StopCoroutine("JSONContentCloud");
                }
			
                switch(DownloadManager.JSONContentCloudState)
                {
                    case 1:
                        mIsContentRequested = true;
                        StartCoroutine(CheckAllDownloadParticular());
                        break;
                    case 2:
                        mIsContentRequested = true;
                        mIsLoadingContent = false;
                        //Debug.Log("Connection Error", "Content Request Failed. Please Check your Internet Connection", LoadContentError);
                        Debug.Log("Content Request Failed. Please Check your Internet Connection");
                        break;
                }
            }
        }*/


        /*IEnumerator CheckAllDownloadParticular()
        {
            if (DownloadManager.JSONContentByDeviceState == 1)
            {
                mObjBuilder.UnActiveAllObjects();
                //Main.debugMsg.text = "Content3DState - " + DownloadManager.Content3DState.ToString();
                string path = DownloadManager.CONTENTS_PATH + "/" + DownloadManager.mCTarget.Contents[this.IndexContent].Name;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                jContent currentContent = DownloadManager.mCTarget.Contents[this.IndexContent];
                TotalDetailContent = currentContent.ContentDetail.Count;

                DownloadManager.DOWNLOAD_CONTENT_TYPE = 0;

                DC = new List<DataContent>();

                for (int i = 0; i < TotalDetailContent; i++)
                {
                    path = DownloadManager.DISK_PATH + "/" + currentContent.Name + "/" + currentContent.ContentDetail[i].Name;
                    if (File.Exists(path))
                    {
                        if (!Utilities.GetMD5Checksum(path).Equals(currentContent.ContentDetail[i].MD5Checksum))
                        {
                            DC.Add(new DataContent() { ContentDetailID = i, ContentID = this.IndexContent, Download = null });
                        }
                    }
                    else
                    {
                        DC.Add(new DataContent() { ContentDetailID = i, ContentID = this.IndexContent, Download = null });
                    }
                }

                if (DC.Count > 0)
                {
                    uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(true);
                    yield return StartCoroutine(DownloadAllContent());
                    coroutineState_AllDownloadParticular = true;
                }
                else
                {
                    uiRoot.transform.Find("DownloadContentPanel").gameObject.SetActive(false);
                    Debug.Log("Direct ReloadAllContent");
                    ReloadAllContent();
                }
            }

        }*/

        //int[,] IndexDownloadParticular = null;
        //bool CheckAllFinishedDownloadParticularState = true;

        /*void CheckAllFinishedDownloadParticular()
        {
            bool[] finishState = new bool[DC.Count];
            Debug.Log("CheckAllFinishedDownloadParticular():" + DC.Count);
            for (int i = 0; i < DC.Count; i++)
            {
                if (DC[i].Download.isDone)
                {
                    finishState[i] = true;
                }
            }

            int finishCount = 0;
            for (int i = 0; i < finishState.Length; i++)
            {
                if (finishState[i]) finishCount++;
            }

            //Debug.Log(finishCount + " vs " + DC.Count);

            if (finishCount == DC.Count)
            {
                CheckAllFinishedDownloadParticularState = false;

                if(DownloadManager.DOWNLOAD_CONTENT_TYPE==0)
                    ReloadAllContent();

                coroutineState_AllDownloadParticular = false;
            }
            else
            {
                CheckAllFinishedDownloadParticularState = true;
                Debug.Log("Download not yet finished");
            }
        }*/



        /*
        IEnumerator VideoPlayer(string path)
        {
            VideoObject.GetComponent<VideoPlaybackBehaviour>().enabled = false;
            VideoObject.GetComponent<MeshRenderer>().enabled = false;
        
            currentVideo.SetPath(path);
            currentVideo.Init();
            yield return new WaitForSeconds(1f);
            VideoObject.GetComponent<VideoPlaybackBehaviour>().enabled = true;
            VideoObject.GetComponent<MeshRenderer>().enabled = true;

        
            yield return new WaitForSeconds(0.5f);
            currentVideo.VideoPlayer.Play(false, currentVideo.VideoPlayer.GetCurrentPosition());
        }
        */

        /*IEnumerator DownloadAllTargets()
        {
            Debug.Log(" DownloadAllTargets ");
            for (int i = 0; i < DT.Count; i++)
            {
                Debug.Log(" " + i);
                //yield return new WaitForSeconds(1.5f);
                GameObject instance = Instantiate(Resources.Load("ProgressBarRAW", typeof(GameObject))) as GameObject;
                instance.name = "ProgressBar_" + i;
                instance.transform.parent = uiRoot.transform.Find("ProgressTarget");
                instance.transform.localPosition = new Vector3(0, Utilities.GetProgressBarYPos(Screen.height, DT.Count, i), 0);
                instance.transform.localScale = new Vector3(2f, 2f, 1f);

                //StartCoroutine(LoadManager.Target(i, DT[i].Index, DT[i].Type));
            }
            Debug.Log("done");
            yield return new WaitForSeconds(0.5f);
            //coroutineState_DownloadAllTargets = true;
        }*/

        /*
        IEnumerator DownloadAllContent()
        {
            for (int i = 0; i < DC.Count; i++)
            {
                //yield return new WaitForSeconds(1.5f);
                GameObject instance = Instantiate(Resources.Load("ProgressBarRAW", typeof(GameObject))) as GameObject;
                instance.name = "ProgressBar_" + i;
                instance.transform.parent = uiRoot.transform.Find("Progress");
                instance.transform.localPosition = new Vector3(0, Utilities.GetProgressBarYPos(Screen.height, DC.Count, i), 0);
                instance.transform.localScale = new Vector3(2f, 2f, 1f);

                //StartCoroutine(DownloadManager.Content(i));
            }
            yield return new WaitForSeconds(0.5f);
            //coroutineState_AllDownloadParticular = true;
        }*/



        void OnSwipe(SwipeGesture gesture)
        {
            /*
            GameObject selection = gesture.StartSelection;
		
            if( selection == swipeObject )
            {
                if(gesture.Velocity>4000f)
                {
                    if(gesture.Direction.ToString().Equals("Left"))
                    {
                        if( (IndexContent+1)==TotalContents )
                            IndexContent = 0;
                        else
                            IndexContent++;
                    }
                    else if(gesture.Direction.ToString().Equals("Right"))
                    {
                        if( (IndexContent-1) == -1 )
                            IndexContent = TotalContents - 1;
                        else
                            IndexContent--;
                    }
                    mIsLoadingTarget = false;
                    mIsLoadingContent = true;
                    mIsContentRequested = false;
                    mIsSwitchContent = true;
				
                    coroutineState_ContentByTarget = false;
                    coroutineState_Content3D = false;
                    DownloadManager.JSONContentByTargetState = 0;
                    Debug.Log ("Current Index : " + IndexContent);
                }


                Debug.Log ("Swiped " + gesture.Direction + " with finger " + gesture.Fingers[0] +
                    " (velocity:" + gesture.Velocity + ", distance: " + gesture.Move.magnitude + " )");

                //Debug.Log( UI.StatusText );
			
                //SwipeParticlesEmitter emitter = selection.GetComponentInChildren<SwipeParticlesEmitter>();
                //if( emitter )
                //	emitter.Emit( gesture.Direction, gesture.Velocity );
            }*/

        }

        



        void OnTap()
        {
            /*if (currentVideo != null)
            {
                VideoPlayerHelper.MediaState state = currentVideo.VideoPlayer.GetStatus();
                if (state == VideoPlayerHelper.MediaState.PAUSED ||
                    state == VideoPlayerHelper.MediaState.READY ||
                    state == VideoPlayerHelper.MediaState.STOPPED)
                {
                    // Pause other videos before playing this one
                    //PauseOtherVideos(currentVideo);

                    // Play this video on texture where it left off
                    currentVideo.VideoPlayer.Play(false, currentVideo.VideoPlayer.GetCurrentPosition());
                }
                else if (state == VideoPlayerHelper.MediaState.REACHED_END)
                {
                    // Pause other videos before playing this one
                    //PauseOtherVideos(currentVideo);

                    // Play this video from the beginning
                    currentVideo.VideoPlayer.Play(false, 0);
                }
                else if (state == VideoPlayerHelper.MediaState.PLAYING)
                {
                    // Video is already playing, pause it
                    currentVideo.VideoPlayer.Pause();
                }
            }*/
        }

        
        /*private void Ping()
        {
            if (!mIsPing)
            {
                if (!coroutineState_Ping)
                {
                    StartCoroutine(DownloadManager.Ping());
                    coroutineState_Ping = true;
                    Debug.Log("StartCoroutine(Ping)");
                }
                if (DownloadManager.PING != 0)
                {
                    Debug.Log("StopCoroutine(Ping)");
                    StopCoroutine("Ping");
                }

                switch (DownloadManager.PING)
                {
                    case 1:
                        Debug.Log("PING!");
                        mIsPing = true;
                        break;
                    case 2:
                        mIsPing = true;
                        //Debug.Log("Connection Error", "Target Request Failed. Please Check your Internet Connection", LoadTargetError);
                        Debug.Log("Send statistic Failed. Please Check your Internet Connection");
                        break;
                }
            }
        }*/


        /*
        IEnumerator AutoPing()
        {
            mIsPing = false;
            coroutineState_Ping = false;
            DownloadManager.PING = 0;
            Ping();
            Debug.Log("Auto Ping");
            yield return new WaitForSeconds(3600);
            StartCoroutine(AutoPing());
        }*/

    }
}