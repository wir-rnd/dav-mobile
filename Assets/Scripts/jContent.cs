﻿using UnityEngine;
using System.Collections.Generic;

public class jContent
{
	public int ContentID { get; set; }
    public string Name { get; set; }
    public string TargetName { get; set; }
    public float px { get; set; }
    public float py { get; set; }
    public float pz { get; set; }
    public float rx { get; set; }
    public float ry { get; set; }
    public float rz { get; set; }
    public float sx { get; set; }
    public float sy { get; set; }
    public float sz { get; set; }
    public List<jContentDetail> ContentDetail { get; set; }

}

public class jContentDetail
{
    public int ContentDetailID { get; set; }
    public string Name { get; set; }
    public string MD5Checksum { get; set; }
}

public class jTargetContent
{
    public string TargetID { get; set; }
    public string Name { get; set; }
}

public class DataContent
{
    public int ContentID { get; set; }
    public int ContentDetailID { get; set; }
    public WWW Download { get; set; }
}