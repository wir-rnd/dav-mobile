﻿using UnityEngine;
using System.Collections;

public class BtNextPrev : MonoBehaviour
{
    public GameObject DavUntrack;
    public Texture2D btActive, btNormal;
    public enum selectMessage
    {
        Next, Prev,
    }
    public selectMessage pesan;

    void Start()
    {
        DavUntrack = GameObject.Find("Dav_UnTrack");
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (GetComponent<GUITexture>().HitTest(Input.mousePosition))
            {
                GetComponent<GUITexture>().texture = btActive;
                Debug.Log("Klik");
                LeanTween.cancel(DavUntrack);
            }


        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (GetComponent<GUITexture>().texture == btActive)
            {
                Debug.Log("Klik Active");
                GetComponent<GUITexture>().texture = btNormal;
                if (!LeanTween.isTweening(DavUntrack)) gameObject.SendMessage(pesan.ToString());
            }
        }
        //print(LeanTween.isTweening(DavUntrack));
    }

    void Next()
    {
        print("Next Gan..");
        if (DavUntrack.transform.localPosition == new Vector3(0.5f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, -1f, 1f).setEase(LeanTweenType.easeOutQuint);
        }
        else if (DavUntrack.transform.localPosition == new Vector3(-1f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, -2f, 1f).setEase(LeanTweenType.easeOutQuint);
        }
        else if (DavUntrack.transform.localPosition == new Vector3(-2f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, -3f, 1f).setEase(LeanTweenType.easeOutQuint);
        }

    }
    void Prev()
    {
        print("Back Gan..");

        if (DavUntrack.transform.localPosition == new Vector3(-3f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, -2f, 1f).setEase(LeanTweenType.easeOutQuint);
        }
        else if (DavUntrack.transform.localPosition == new Vector3(-1f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, 0.5f, 1f).setEase(LeanTweenType.easeOutQuint);
        }
        else if (DavUntrack.transform.localPosition == new Vector3(-2f, 0.5f, 5f))
        {
            LeanTween.moveLocalX(DavUntrack, -1f, 1f).setEase(LeanTweenType.easeOutQuint);
        }
    }

}