﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System;
using DAV;


public class Utilities
{
    public static void SyncDirectory(List<string> source)
    {
        for (int i = 0; i < source.Count; i++)
        {
            if (!Directory.Exists(DAV.Global.CONTENTS_PATH + "/" + source[i]))
                Directory.CreateDirectory(DAV.Global.CONTENTS_PATH + "/" + source[i]);
        }

        string[] dest = Directory.GetDirectories(DAV.Global.CONTENTS_PATH);
        List<string> lDest = new List<string>();
        string backslash = @"\";
        for (int i = 0; i < dest.Length; i++)
        {
            string[] split = dest[i].Split(backslash[0]);
            lDest.Add(split[split.Length-1]);
        }

        for (int i = 0; i < lDest.Count; i++)
        {
            if (!source.Contains(lDest[i]))
            {
                //Debug.Log("Deleted dir - " + DAV.Global.CONTENTS_PATH + "/" + lDest[i]);
                Directory.Delete(DAV.Global.CONTENTS_PATH + "/" + lDest[i], true);
            }
        }

    }

    public static string GetMD5Checksum(string path)
    {
        using (var md5 = MD5.Create())
        {
            using (var stream = File.OpenRead(path))
            {
                return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
            }
        }
    }

    public static float GetProgressBarYPos(float h, int tc, int idx)
    {
        float x = h / float.Parse(tc.ToString());
        float y = x / 2f;
        return (x * idx) + y - (h / 2f);
    }

    public static bool IsJson(string jsonData)
    {
        return jsonData.Trim().Substring(0, 1).IndexOfAny(new[] { '[', '{' }) == 0;
    }
    public static string CleanMethodAssemblyName(string name)
    {
        name = name.Split('<')[1];
        name = name.Split('>')[0];
        return name;
    }

    public static bool FileCheck(string path, string MD5)
    {
        bool res = false;
        if (File.Exists(path))
        {
            res = true;
            //Debug.Log(Utilities.GetMD5Checksum(path) + " " + MD5);
            if (!Utilities.GetMD5Checksum(path).Equals(MD5))
                res = false;
            else
                res = true;
        }
        else
            res = false;
        return res;
    }

}

