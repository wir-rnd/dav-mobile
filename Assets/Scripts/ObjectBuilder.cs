﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ObjectBuilder
{
	private GameObject mObject;
	private GameObject mObject3D;
    private string mName;
    private Assembly mAsm;
    private bool mIsAssemblySet = false;
    //private Vector3 mDefaultScale = new Vector3(20f, 20f, 20f);
    private GameObject mContent, mTarget;
	public void SetRootObject(GameObject obj) // 3D object which had already loaded before cloud recognition found target
	{
		mObject = obj;
		//mDesc = this.mObject.transform.Find("Desc").GetComponent<TextMesh>();
	}

    /*public GameObject GetObject3D()
    {
        return this.mObject3D;
    }

    public void SetTarget(GameObject obj)
    {
        mTarget = obj;
    }
    public void SetContent(GameObject obj)
    {
        mContent = obj;
    }

	public void BuildObject(AssetBundle asset)
	{
        //Main.debugMsg.text = "aaaaaa - " + DownloadManager.Content3DState.ToString();
        try
        {
            asset.GetInstanceID();
        }
        catch(System.Exception E)
        {
            //DAV.Main.debugMsg.text = E.ToString();
        }

		if(asset.mainAsset != null )
		{
            asset.GetInstanceID();
            //UnityEngine.Object[] a = asset.LoadAll();
            //var tai = GameObject.Instantiate(asset.mainAsset);
            Debug.Log("mainAsset.name : " + asset.mainAsset.name);
			//this.mObject3D = GameObject.Find(this.mName+"(Clone)") as GameObject;
            this.mObject3D = GameObject.Instantiate(asset.mainAsset) as GameObject;
			this.mObject3D.name = this.mName;

            this.mObject3D.transform.parent = mContent.transform;


			this.mObject3D.SetActive(false);

            asset.Unload(false);
		} else {
            //Main.debugMsg.text = "hhhhhh - " + DownloadManager.Content3DState.ToString(); 
			Debug.Log ("BuildObject Failed");
		}
	}

    public void AssemblyObjectToTarget(string content_name, string target_name)
    {
        GameObject go = GameObject.Instantiate(mContent.transform.Find(content_name).gameObject) as GameObject;
        go.name = content_name;
        go.transform.parent = mTarget.transform.Find(target_name);
        Debug.Log("AssemblyObjectToTarget(" + content_name + ", " + target_name + ")");
    }*/

	public void SetName(string name)
	{
		this.mName = name;
	}

	public void SetActiveObject(bool state)
	{
		this.mObject.transform.Find(this.mName).gameObject.SetActive(state);
	}

	public void UnActiveAllObjects()
	{	/*
		for(int i=0;i<DownloadManager.mCDevice.Contents.Count;i++)
		{
			try
			{
                this.mObject.transform.Find(DownloadManager.mCDevice.Contents[i].Name).gameObject.SetActive(false);
			}
			catch {}
		}*/
	}

    public void SetScale(float x, float y, float z)
	{
		this.mObject.transform.Find(this.mName).localScale = new Vector3 (x, y, z);
	}

    public void SetDefaultScale(float x, float y, float z)
    {
        //this.mDefaultScale = new Vector3(x, y, z);
    }

    public void SetPosition(float x, float y, float z)
    {
        this.mObject.transform.Find(this.mName).localPosition = new Vector3(x, y, z);
    }

    public void SetRotation(float x, float y, float z)
    {
        Quaternion q = Quaternion.Euler(x, y, z);
        //q.eulerAngles = new Vector3(x, y, z);
        
        this.mObject.transform.Find(this.mName).localRotation = q;
    }
	
	public void DestroyObject()
	{
		UnityEngine.Object.Destroy(this.mObject.transform.Find(this.mName).gameObject);
	}

	public bool CheckObject(string name)
	{
		try
		{
			bool res = false;
			if(this.mObject.transform.Find(name).gameObject!=null) 
			{
				res = true;
			}
			return res;
		}
		catch
		{
			return false;
		}
	}

    public void RebuildObject(string name)
    {
        //Debug.Log(name +" :::: " + tracker_name);
        if (this.mContent.transform.Find(name)!=null)
            this.mContent.transform.Find(name).parent = this.mObject.transform;
    }

    public bool AssemblySet
    {
        set
        {
            this.mIsAssemblySet = value;
        }
        get
        {
            return this.mIsAssemblySet;
        }
    }


    public void BuildAssembly(byte[] asmByte)
    {
        if (this.AssemblySet)
        {
            this.mAsm = Assembly.Load(asmByte);
        }
    }

    public void LoadAssembly()
    {
        if (this.AssemblySet)
        {
            foreach (System.Type type in this.mAsm.GetTypes())
            {
                Debug.Log("Type : " + type.Name);
                MethodInfo mi = type.GetMethod("GetAssemblyPath");
                object instance = Activator.CreateInstance(type);
                FastInvokeHandler fastInvoker = FastMethodInvoker.GetMethodInvoker(mi);
                string[] result = (string[])fastInvoker(instance, null);

                //Debug.Log("GetAssemblyPath : " + result);
                if(result!=null)
                {
                    for (int i = 0; i < result.Length;i++)
                    {
                        if (!result[i].Equals(string.Empty))
                        {
                            if (this.mContent.transform.Find(this.mName).Find(result[i].ToString()).gameObject.GetComponent(type) == null)
                            {
                                this.mContent.transform.Find(this.mName).Find(result[i].ToString()).gameObject.AddComponent(type);
                                Debug.Log("CCCCCCCCCCCC");
                            }
                            else
                            {
                                GameObject.Destroy(this.mContent.transform.Find(this.mName).Find(result[i].ToString()).gameObject.GetComponent(type));
                                this.mContent.transform.Find(this.mName).Find(result[i].ToString()).gameObject.AddComponent(type);
                                Debug.Log("DDDDDDDDDDDDDDDDD");
                            }
                        }
                        else
                        {
                            /*if (this.mContent.transform.Find(this.mName).GetComponent(type) == null)
                            {
                                this.mContent.transform.Find(this.mName).gameObject.AddComponent(type);
                                Debug.Log("AAAAAAAAAAAAAAAAAA");
                            }
                            else
                            {
                                Component[] components = this.mContent.transform.Find(this.mName).GetComponents(type);
                                foreach(Component comp in components)
                                {
                                    GameObject.Destroy(comp);
                                }
                                yield return new WaitForSeconds(2);
                                this.mContent.transform.Find(this.mName).gameObject.AddComponent(type);
                                Debug.Log("BBBBBBBBBBBBBBBBBB");
                            }*/
                            Component[] components = this.mContent.transform.Find(this.mName).GetComponents(type);
                            foreach (Component comp in components) GameObject.Destroy(comp);
                            this.mContent.transform.Find(this.mName).gameObject.AddComponent(type);
                            for (int j = 0; j < mTarget.transform.childCount;j++)
                            {
                                for(int k=0;k<mTarget.transform.GetChild(j).childCount;k++)
                                {
                                    components = mTarget.transform.GetChild(j).GetChild(k).GetComponents(type);
                                    foreach (Component comp in components) GameObject.Destroy(comp);
                                    mTarget.transform.GetChild(j).GetChild(k).gameObject.AddComponent(type);
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //MonoBehaviour.Destroy(mObject3D.GetComponent(type));
        }
    }

    public void UnloadAssembly()
    {
        if (this.AssemblySet)
        {
            //this.mObject3D.AddComponent(this.mAsm.GetType());
            foreach (System.Type type in this.mAsm.GetTypes())
            {
                MethodInfo mi = type.GetMethod("GetAssemblyPath");
                object instance = Activator.CreateInstance(type);
                FastInvokeHandler fastInvoker = FastMethodInvoker.GetMethodInvoker(mi);
                object result = fastInvoker(instance, null);
                Debug.Log("GetAssemblyPath : " + result);
                if (!result.ToString().Equals(""))
                {
                    MonoBehaviour.Destroy(this.mContent.transform.Find(this.mName).Find(result.ToString()).gameObject.GetComponent(typeof(ScriptableObject)));
                }
                    
                else
                    this.mContent.transform.Find(this.mName).GetComponent(type);
            }
        }
    }
}
